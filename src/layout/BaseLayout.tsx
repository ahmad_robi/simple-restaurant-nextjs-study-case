import { Montserrat } from "next/font/google";

const montserrat = Montserrat({
  subsets: ["latin"],
  variable: "--font-montserrat",
});

const BaseLayout = ({ children }: { children: React.ReactNode }) => {
  return <main className={montserrat.variable}>{children}</main>;
};

export default BaseLayout;
