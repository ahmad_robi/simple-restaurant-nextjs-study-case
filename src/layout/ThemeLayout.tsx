import { useTheme } from "@/context/ThemeContext";
import BaseLayout from "./BaseLayout";

const ThemeLayout = ({ children }: { children: React.ReactNode }) => {
  const { theme } = useTheme();

  return (
    <BaseLayout>
      <div className={theme == "dark" ? "dark" : "light"}>{children}</div>
    </BaseLayout>
  );
};

export default ThemeLayout;
